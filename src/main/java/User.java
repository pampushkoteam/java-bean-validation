import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

/**
 *
 */
public class User
{
	@NotNull(message = "Name cannot be null")
	private String name;
	
	@NotNull(message = "Фамилия не заполнена, заполните её!")
	private String lastname;
	
	@AssertTrue(message = "Эй, это не джава программист")
	private boolean isJavaProgrammer;
	
	public User(@NotNull(message = "Name cannot be null") String name)
	{
		this.name = name;
	}
	
	//********************* getters and setters *********************//
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String getLastname()
	{
		return lastname;
	}
	
	public void setLastname(String lastname)
	{
		this.lastname = lastname;
	}
	
	public boolean isJavaProgrammer()
	{
		return isJavaProgrammer;
	}
	
	public void setJavaProgrammer(boolean javaProgrammer)
	{
		isJavaProgrammer = javaProgrammer;
	}
}
