import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

/**
 *
 */
public class Main
{
	public static void main(String[] args)
	{
		User user = new User("Василий");
		//фамилию мы не заполняем (устанавливаем проверку на заполненность),
		//значение свойства isJavaProgrammer мы оставляем равным false (устанавливаем проверку, генерирующую ошибку при значении не равном true)
		
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		Set<ConstraintViolation<User>> violations = validator.validate(user);
		for (ConstraintViolation<User> violation : violations)
		{
			System.out.println(violation.getMessage());
		}
		
	}
}
